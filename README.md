# Proyecto Final/Physics Squad

## Introducción a la Computación

> Por: Gabriel Salandra

Este es mi proyecto final de la materia Introducción a la Computación. Se trata de una página interactiva que nos ayude a resolver problemas de física. Los temas con los que se trabajan son:

- Cinemática
- Conservación de la Energía
- Calculador en Javascript
