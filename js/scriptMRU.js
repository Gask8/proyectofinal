//Javascript de la MRU

var respuesta_final
var valor_velocidad
var valor_distancia
var valor_tiempo
var formula

function validar1(){
   var tipo_cover = document.getElementById("incognita1").value;
   formula = 'A'
   //window.alert(formula);
        document.getElementById("valorv").disabled = true ;
        document.getElementById("valorv").value = 'N/A' ;
        document.getElementById("valord").disabled = false ;
        document.getElementById("valort").disabled = false ;
        document.getElementById("resu").value = "m/s";
        document.getElementById("resu").style.display = 'none';
        document.getElementById("resp").value = '';
  }

  function validar2(){
     var tipo_cover = document.getElementById("incognita2").value;
     formula = 'B'
     //window.alert(tipo_cover);
          document.getElementById("valorv").disabled = false ;
          document.getElementById("valord").disabled = true ;
          document.getElementById("valord").value = 'N/A' ;
          document.getElementById("valort").disabled = false ;
          document.getElementById("resu").value = "m";
          document.getElementById("resu").style.display = 'none';
          document.getElementById("resp").value = '';
    }

    function validar3(){
       var tipo_cover = document.getElementById("incognita3").value;
       formula = 'C'
       //window.alert(tipo_cover);
            document.getElementById("valorv").disabled = false ;
            document.getElementById("valord").disabled = false ;
            document.getElementById("valort").disabled = true ;
            document.getElementById("valort").value = 'N/A' ;
            document.getElementById("resu").value = "s";
            document.getElementById("resu").style.display = 'none';
            document.getElementById("resp").value = '';
      }

function resolver(){
  var valor_velocidad = document.getElementById("valorv").value;
  var valor_distancia = document.getElementById("valord").value;
  var valor_tiempo = document.getElementById("valort").value;
  switch(formula){
    case 'A':
    respuesta_final = valor_distancia / valor_tiempo;
    respuesta_final = respuesta_final.toFixed(2);
    document.getElementById("resp").value = respuesta_final;
    document.getElementById("resu").style.display = 'inline';
    break;
    case 'B':
    respuesta_final = valor_velocidad * valor_tiempo;
    respuesta_final = respuesta_final.toFixed(2);
    document.getElementById("resp").value = respuesta_final;
    document.getElementById("resu").style.display = 'inline';
    break;
    case 'C':
    respuesta_final = valor_distancia / valor_velocidad;
    respuesta_final = respuesta_final.toFixed(2);
    document.getElementById("resp").value = respuesta_final;
    document.getElementById("resu").style.display = 'inline';
    break;
    default:
    window.alert("Introdusca bien los datos");
    break;
  }
}

function problema1(){
  var valor_verdadero;
  valor_verdadero = document.getElementById("problema1").value;
  if (valor_verdadero == 83.3){
    window.alert("Correcto");
  } else {
    window.alert("Falso");
  }
}
function problema2(){
  var valor_verdadero;
  valor_verdadero = document.getElementById("problema2").value;
  if (valor_verdadero == 15){
    window.alert("Correcto");
  } else {
    window.alert("Falso");
  }
}
