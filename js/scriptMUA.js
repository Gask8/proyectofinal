//Javascript de la MUA

var respuesta_final;
var alter_respuesta;
var valor_velocidad1;
var valor_velocidad2;
var valor_aceleracion;
var valor_distancia;
var valor_tiempo;
var formula;
var caidabol = false;

function caidal(){
  if (caidabol == false) {
caidabol = true;
document.getElementById("valora").disabled = true ;
document.getElementById("valora").value = 9.81 ;
document.getElementById("incognita5").disabled = true ;
} else {
caidabol = false;
document.getElementById("incognita5").disabled = false ;
document.getElementById("valora").disabled = false ;
}
  //window.alert(caidabol);
}

function validar1(){
   var tipo_cover = document.getElementById("incognita1").value;
   formula = 'A'
   //window.alert(formula);
   document.getElementById("valorv1").disabled = true ;
   document.getElementById("valorv1").value = 'N/A' ;
   document.getElementById("valorv2").disabled = false ;
   document.getElementById("valord").disabled = false ;
   document.getElementById("valort").disabled = false ;
   document.getElementById("resu").value = "m/s";
   document.getElementById("resu").style.display = 'none';
   document.getElementById("resp").value = '';
   document.getElementById("resu2").style.display = 'none';
   document.getElementById("resu3").style.display = 'none';
   document.getElementById("resp2").value = '';
   if (caidabol == false){
        document.getElementById("valora").disabled = false ;
  }
}

  function validar2(){
     var tipo_cover = document.getElementById("incognita2").value;
     formula = 'B'
     //window.alert(tipo_cover);
     document.getElementById("valorv1").disabled = false ;
     document.getElementById("valorv2").disabled = true ;
     document.getElementById("valorv2").value = 'N/A' ;
     document.getElementById("valord").disabled = false ;
     document.getElementById("valort").disabled = false ;
     document.getElementById("resu").value = "m/s";
     document.getElementById("resu").style.display = 'none';
     document.getElementById("resp").value = '';
     document.getElementById("resu2").style.display = 'none';
     document.getElementById("resu3").style.display = 'none';
     document.getElementById("resp2").value = '';
     if (caidabol == false){
          document.getElementById("valora").disabled = false ;
    }
  }

    function validar3(){
       var tipo_cover = document.getElementById("incognita3").value;
       formula = 'C'
       //window.alert(tipo_cover);
       document.getElementById("valorv1").disabled = false ;
       document.getElementById("valorv2").disabled = false ;
       document.getElementById("valord").disabled = true ;
       document.getElementById("valord").value = 'N/A' ;
       document.getElementById("valort").disabled = false ;
       document.getElementById("resu").value = "m";
       document.getElementById("resu").style.display = 'none';
       document.getElementById("resp").value = '';
       document.getElementById("resu2").style.display = 'none';
       document.getElementById("resu3").style.display = 'none';
       document.getElementById("resp2").value = '';
       if (caidabol == false){
            document.getElementById("valora").disabled = false ;
      }
    }

      function validar4(){
         var tipo_cover = document.getElementById("incognita4").value;
         formula = 'D'
         //window.alert(tipo_cover);
         document.getElementById("valorv1").disabled = false ;
         document.getElementById("valorv2").disabled = false ;
         document.getElementById("valord").disabled = false ;
         document.getElementById("valort").disabled = true ;
         document.getElementById("valort").value = 'N/A' ;
         document.getElementById("resu").value = "s";
         document.getElementById("resu").style.display = 'none';
         document.getElementById("resp").value = '';
         document.getElementById("resu2").style.display = 'none';
         document.getElementById("resu3").style.display = 'none';
         document.getElementById("resp2").value = '';
         if (caidabol == false){
              document.getElementById("valora").disabled = false ;
        }
      }

        function validar5(){
           var tipo_cover = document.getElementById("incognita5").value;
           formula = 'E'
           //window.alert(tipo_cover);
           document.getElementById("valorv1").disabled = false ;
           document.getElementById("valorv2").disabled = false ;
           document.getElementById("valord").disabled = false ;
           document.getElementById("valort").disabled = false ;
           document.getElementById("valora").disabled = true ;
           document.getElementById("valora").value = 'N/A' ;
           document.getElementById("resu").value = "m/s2";
           document.getElementById("resu").style.display = 'none';
           document.getElementById("resp").value = '';
           document.getElementById("resu2").style.display = 'none';
           document.getElementById("resu3").style.display = 'none';
           document.getElementById("resp2").value = '';
         }

function resolver(){
  var vv1b = false;
  var vv2b = false;
  var vdb = false;
  var vtb = false;
  var vab = false;

  var valor_velocidad1 = document.getElementById("valorv1").value;
  var valor_velocidad2 = document.getElementById("valorv2").value;
  var valor_distancia = document.getElementById("valord").value;
  var valor_tiempo = document.getElementById("valort").value;
  var valor_aceleracion = document.getElementById("valora").value;
  //window.alert(valor_tiempo);
  if (valor_velocidad1 == ""||valor_velocidad1 == "N/A"){
    vv1b = true;} else {vv1b = false;}
  if (valor_velocidad2 == ""||valor_velocidad2 == "N/A"){
    vv2b = true;} else {vv2b = false;}
  if (valor_distancia == ""|| valor_distancia == "N/A"){
    vdb = true;} else {vdb = false;}
  if (valor_tiempo == ""||valor_tiempo == "N/A"){
    vtb = true;} else {vtb = false;}
  if (valor_aceleracion == ""||valor_aceleracion == "N/A"){
    vab = true;} else {vab = false;}
  switch(formula){
    case 'A':
      if (vv2b) {
        respuesta_final = (valor_distancia - (0.5*valor_aceleracion*(Math.pow(valor_tiempo,2)))) / valor_tiempo;
        document.getElementById("resu3").value = 'm/s'
        document.getElementById("resu2").value = 'Velocidad Final = ';
        alter_respuesta = respuesta_final + (valor_tiempo*valor_aceleracion);
      } else if (vdb) {
        respuesta_final = valor_velocidad2 - (valor_aceleracion*valor_tiempo);
        document.getElementById("resu3").value = 'm'
        document.getElementById("resu2").value = 'Distancia = ';
        alter_respuesta = ((Math.pow(valor_velocidad2,2)) - (Math.pow(respuesta_final,2))) / (2*valor_aceleracion);
      } else if (vtb) {
        respuesta_final = Math.sqrt((Math.pow(valor_velocidad2,2))-(2*valor_aceleracion*valor_distancia));
        document.getElementById("resu3").value = 's'
        document.getElementById("resu2").value = 'Tiempo = ';
        alter_respuesta = (valor_velocidad2 - respuesta_final) / (valor_aceleracion);
      } else {
        respuesta_final = ((2*valor_distancia)/valor_tiempo) - valor_velocidad2;
        document.getElementById("resu3").value = 'm/s2'
        document.getElementById("resu2").value = 'Aceleración = ';
        alter_respuesta = (valor_velocidad2 - respuesta_final) / (valor_tiempo);
      }
    break;
    case 'B':
    if (vv1b) {
      respuesta_final = (valor_distancia + (0.5*valor_aceleracion*(Math.pow(valor_tiempo,2)))) / valor_tiempo;
      document.getElementById("resu3").value = 'm/s'
      document.getElementById("resu2").value = 'Velocidad Inicial = ';
      alter_respuesta = respuesta_final - (valor_tiempo*valor_aceleracion);
    } else if (vdb) {
      document.getElementById("resu3").value = 'm'
      document.getElementById("resu2").value = 'Distancia = ';
      respuesta_final = (valor_velocidad1) + (valor_tiempo*valor_aceleracion);
      alter_respuesta = ((Math.pow(respuesta_final,2)) - (Math.pow(valor_velocidad1,2))) / (2*valor_aceleracion);
    } else if (vtb) {
      respuesta_final = Math.sqrt((Math.pow(valor_velocidad1,2))+(2*valor_aceleracion*valor_distancia));
      document.getElementById("resu3").value = 's'
      document.getElementById("resu2").value = 'Tiempo = ';
      alter_respuesta = (respuesta_final - valor_velocidad1) / (valor_aceleracion);
    } else {
      respuesta_final = ((2*valor_distancia)/valor_tiempo) - valor_velocidad1;
      document.getElementById("resu3").value = 'm/s2'
      document.getElementById("resu2").value = 'Aceleración = ';
      alter_respuesta = (respuesta_final - valor_velocidad1) / (valor_tiempo);
    }
    break;
    case 'C':
    if (vv1b) {
      respuesta_final = (valor_velocidad2*valor_tiempo) - (0.5*valor_aceleracion*(Math.pow(valor_tiempo,2)));
      document.getElementById("resu3").value = 'm/s'
      document.getElementById("resu2").value = 'Velocidad Inicial = ';
      alter_respuesta = valor_velocidad2 - (valor_tiempo*valor_aceleracion);
    } else if (vv2b) {
      respuesta_final = (valor_velocidad1*valor_tiempo) + (0.5*valor_aceleracion*(Math.pow(valor_tiempo,2)));
      document.getElementById("resu3").value = 'm/s'
      document.getElementById("resu2").value = 'Velocidad Final = ';
      alter_respuesta = valor_velocidad1 + (valor_tiempo*valor_aceleracion);
    } else if (vtb) {
      respuesta_final = ((Math.pow(valor_velocidad2,2))-(Math.pow(valor_velocidad1,2)))/(2*valor_aceleracion);
      document.getElementById("resu3").value = 's'
      document.getElementById("resu2").value = 'Tiempo = ';
      alter_respuesta = (valor_velocidad2 - valor_velocidad1) / (valor_aceleracion);
    } else {
      respuesta_final = ((valor_velocidad2 + valor_velocidad1)/2)*valor_tiempo;
      document.getElementById("resu3").value = 'm/s2'
      document.getElementById("resu2").value = 'Aceleración = ';
      alter_respuesta = (valor_velocidad2 - valor_velocidad1) / (valor_tiempo);
    }
    case 'D':
    if (vv2b) {
      respuesta_final = (valor_distancia - (0.5*valor_aceleracion*(Math.pow(valor_tiempo,2)))) / valor_tiempo;
      document.getElementById("resu3").value = 'm/s'
      document.getElementById("resu2").value = 'Velocidad Final = ';
      alter_respuesta = respuesta_final + (valor_tiempo*valor_aceleracion);
    } else if (vdb) {
      respuesta_final = valor_velocidad2 - (valor_aceleracion*valor_tiempo);
      document.getElementById("resu3").value = 'm'
      document.getElementById("resu2").value = 'Distancia = ';
      alter_respuesta = ((Math.pow(valor_velocidad2,2)) - (Math.pow(respuesta_final,2))) / (2*valor_aceleracion);
    } else if (vtb) {
      respuesta_final = Math.sqrt((Math.pow(valor_velocidad2,2))-(2*valor_aceleracion*valor_distancia));
      document.getElementById("resu3").value = 's'
      document.getElementById("resu2").value = 'Tiempo = ';
      alter_respuesta = (valor_velocidad2 - respuesta_final) / (valor_aceleracion);
    } else {
      respuesta_final = ((2*valor_distancia)/valor_tiempo) - valor_velocidad2;
      document.getElementById("resu3").value = 'm/s2'
      document.getElementById("resu2").value = 'Aceleración = ';
      alter_respuesta = (valor_velocidad2 - respuesta_final) / (valor_tiempo);
    }
    case 'E':
    if (vv1b) {
      respuesta_final = (2*valor_velocidad2*valor_tiempo-2*valor_distancia)/(Math.pow(valor_tiempo,2));
      document.getElementById("resu3").value = 'm/s'
      document.getElementById("resu2").value = 'Velocidad Inicial = ';
      alter_respuesta = valor_velocidad2 - (valor_tiempo*respuesta_final);
    } else if (vv2b) {
      respuesta_final = (2*valor_distancia-2*valor_velocidad1*valor_tiempo)/(Math.pow(valor_tiempo,2));
      document.getElementById("resu3").value = 'm/s'
      document.getElementById("resu2").value = 'Velocidad Inicial = ';
      alter_respuesta = valor_velocidad1 + (valor_tiempo*respuesta_final);
    } else if (vdb) {
      respuesta_final = (valor_velocidad2-valor_velocidad1)/valor_tiempo;
      document.getElementById("resu3").value = 'm'
      document.getElementById("resu2").value = 'Distancia = ';
      alter_respuesta = valor_velocidad1*valor_tiempo + (0.5*respuesta_final*(Math.pow(valor_tiempo,2)))
    } else {
      respuesta_final = ((Math.pow(valor_velocidad2,2)) - (Math.pow(valor_velocidad1,2))) / (2*valor_distancia);
      document.getElementById("resu3").value = 's'
      document.getElementById("resu2").value = 'Tiempo = ';
      alter_respuesta = (valor_velocidad2 - respuesta_final) / (respuesta_final);
    }
    break;
    default:
    window.alert("Introdusca bien los datos");
    break;
  }
  respuesta_final = respuesta_final.toFixed(2);
  alter_respuesta = alter_respuesta.toFixed(2);
  document.getElementById("resp").value = respuesta_final;
  document.getElementById("resp2").value = alter_respuesta;
  document.getElementById("resu").style.display = 'inline';
  document.getElementById("resu2").style.display = 'inline';
  document.getElementById("resu3").style.display = 'inline';
}

function problema1(){
  var valor_verdadero;
  valor_verdadero = document.getElementById("problema1").value;
  if (valor_verdadero == 99.1){
    window.alert("Correcto");
  } else {
    window.alert("Falso");
  }
}
function problema2(){
  var valor_verdadero;
  valor_verdadero = document.getElementById("problema2").value;
  if (valor_verdadero == 20){
    window.alert("Correcto");
  } else {
    window.alert("Falso");
  }
}
