//Javascript de la Pagina
function saludo(){
  window.alert("¡Si, se puede!");
}

//Convertidor
var numero_convertido;
var tipo_de_conversion;

function oculta(){
  document.getElementById("blank").style.display = 'inline';
  document.getElementById("converdistancia").style.display = 'none';
  document.getElementById("convertiempo").style.display = 'none';
  document.getElementById("convervelocidad").style.display = 'none';
}

function validaconver(){
 var tipo_cover = document.getElementById("convertype").value;
 //window.alert(tipo_cover);
 switch(tipo_cover){
    case 'A':
      document.getElementById("blank").style.display = 'none';
      document.getElementById("converdistancia").style.display = 'inline';
      document.getElementById("convertiempo").style.display = 'none';
      document.getElementById("convervelocidad").style.display = 'none';
      break;
    case 'B':
      document.getElementById("blank").style.display = 'none';
      document.getElementById("converdistancia").style.display = 'none';
      document.getElementById("convertiempo").style.display = 'inline';
      document.getElementById("convervelocidad").style.display = 'none';
      break;
    case 'C':
      document.getElementById("blank").style.display = 'none';
      document.getElementById("converdistancia").style.display = 'none';
      document.getElementById("convertiempo").style.display = 'none';
      document.getElementById("convervelocidad").style.display = 'inline';
      break;
    default:
      window.alert("Error");
      break;
  }
}

function validadistancia(){
 var tipo_cover = document.getElementById("converB.1").value;
 //window.alert(tipo_cover);
 switch(tipo_cover){
    case 'm-km':
    document.getElementById("u1").value = "m";
    document.getElementById("u2").value = "km";
    tipo_de_conversion = "A.1";
      break;
    case 'km-m':
    document.getElementById("u1").value = "km";
    document.getElementById("u2").value = "m";
    tipo_de_conversion = "A.2";
      break;
    case 'm-cm':
    document.getElementById("u1").value = "m";
    document.getElementById("u2").value = "cm";
    tipo_de_conversion = "A.3";
      break;
    case 'cm-m':
      document.getElementById("u1").value = "cm";
      document.getElementById("u2").value = "m";
      tipo_de_conversion = "A.4";
        break;
    case 'mi-m':
      document.getElementById("u1").value = "mi";
      document.getElementById("u2").value = "m";
      tipo_de_conversion = "A.6";
          break;
    case 'm-mi':
      document.getElementById("u1").value = "m";
      document.getElementById("u2").value = "mi";
      tipo_de_conversion = "A.5";
            break;
    default:
      window.alert("Error");
      break;
  }
}

function validatiempo(){
 var tipo_cover = document.getElementById("converB.2").value;
 //window.alert(tipo_cover);
 switch(tipo_cover){
    case 's-min':
    document.getElementById("u1").value = "s";
    document.getElementById("u2").value = "min";
    tipo_de_conversion = "B.1";
      break;
    case 'min-s':
    document.getElementById("u1").value = "min";
    document.getElementById("u2").value = "s";
    tipo_de_conversion = "B.2";
      break;
    case 's-h':
    document.getElementById("u1").value = "s";
    document.getElementById("u2").value = "h";
    tipo_de_conversion = "B.3";
      break;
    case 'h-s':
      document.getElementById("u1").value = "h";
      document.getElementById("u2").value = "s";
      tipo_de_conversion = "B.4";
        break;
    default:
      window.alert("Error");
      break;
  }
}

function validavelocidad(){
 var tipo_cover = document.getElementById("converB.3").value;
 //window.alert(tipo_cover);
 switch(tipo_cover){
    case 'm/s-km/h':
    document.getElementById("u1").value = "m/s";
    document.getElementById("u2").value = "km/h";
    tipo_de_conversion = "C.1";
      break;
    case 'km/h-m/s':
    document.getElementById("u1").value = "km/h";
    document.getElementById("u2").value = "m/s";
    tipo_de_conversion = "C.2";
      break;
    case 'm/s-mi/h':
    document.getElementById("u1").value = "m/s";
    document.getElementById("u2").value = "mi/h";
    tipo_de_conversion = "C.3";
      break;
    case 'mi/h-m/s':
      document.getElementById("u1").value = "mi/h";
      document.getElementById("u2").value = "m/s";
      tipo_de_conversion = "C.4";
        break;
    default:
      window.alert("Error");
      break;
  }
}

function convertir(){
  var numero_aconvertir = document.getElementById("nconvertir").value;
  switch(tipo_de_conversion){
     case 'A.1':
     numero_convertido = numero_aconvertir / (1000);
       break;
       case 'A.2':
       numero_convertido = numero_aconvertir * (1000);
         break;
       case 'A.3':
       numero_convertido = numero_aconvertir * (100);
         break;
       case 'A.4':
       numero_convertido = numero_aconvertir / (100);
         break;
       case 'A.5':
       numero_convertido = numero_aconvertir / (1609.3);
         break;
       case 'A.6':
        numero_convertido = numero_aconvertir * (1609.3);
         break;
        case 'B.1':
         numero_convertido = numero_aconvertir / (60);
         break;
        case 'B.2':
          numero_convertido = numero_aconvertir * (60);
          break;
        case 'B.3':
          numero_convertido = numero_aconvertir / (3600);
          break;
        case 'B.4':
          numero_convertido = numero_aconvertir * (3600);
          break;
        case 'C.1':
          numero_convertido = (numero_aconvertir * (3600)) / (1000);
          break;
        case 'C.2':
          numero_convertido = (numero_aconvertir * (1000)) / (3600);
          break;
        case 'C.3':
          numero_convertido = (numero_aconvertir * (3600)) / (1609.3);
          break;
        case 'C.4':
          numero_convertido = (numero_aconvertir * (1609.3)) / (3600);
          break;
     default:
       window.alert("Error");
       break;
   }
   numero_convertido = numero_convertido.toFixed(2);
   document.getElementById("nconvertido").value = numero_convertido;
}
